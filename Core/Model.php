<?php

namespace Core;

use mysqli;
use App\Config;

/**
 * Base model
 */
abstract class Model {
    /**
     * Get the mysql database connection
     * 
     * @return static
     */
    protected static function getDB() {
        static $conn = null;
        if ( $conn === null ) {
            $db_host = Config::DB_HOST;
            $db_user = Config::DB_USER;
            $db_password = Config::DB_PASSWORD;
            $conn = new mysqli($db_host, $db_user, $db_password);
            if ( $conn -> connect_error ) {
                die ("Not conection" . $conn -> connect_error);
            }

            $db_name = Config::DB_NAME;
            $db_selected = mysqli_select_db($conn, $db_name);
            if ( !$db_selected ) {
                $createDB = "CREATE DATABASE IF NOT EXISTS $db_name character set UTF8 collate utf8_general_ci";
                if ( mysqli_query($conn, $createDB) ) {
                    mysqli_select_db($conn, $db_name);
                } else {
                    echo 'Error creating database: ' . mysqli_error($conn);
                }
            }

            return $conn;

            mysqli_close($conn);
        }
    }
} 