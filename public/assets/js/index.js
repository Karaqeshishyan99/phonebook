const addPhoneElem = document.querySelector('.add-phone');
if (addPhoneElem) {
	addPhoneElem.onclick = function() {
		addPhone();
	};

	const phoneNumbersElem = document.querySelector('.phone-numbers');
	addPhone = () => {
		let newPhone = document.createElement('div');
		let newInput = document.createElement('input');
		let newIcon = document.createElement('i');
		newIcon.setAttribute('class', 'fas fa-minus-circle delete-input-icon');
		newIcon.addEventListener(
			'click',
			(deleteInput = () => {
				newPhone.parentNode.removeChild(newPhone);
			})
		);
		newInput.setAttribute('type', 'number');
		newInput.setAttribute('name', 'phones[]');
		newInput.setAttribute('placeholder', 'Phone');
		newInput.setAttribute('class', 'delete-input');
		newPhone.appendChild(newInput);
		newPhone.appendChild(newIcon);
		phoneNumbersElem.appendChild(newPhone);
	};
}

deleteIcon = (elem, e) => {
	elem.parentElement.parentElement.removeChild(elem.parentElement);
};
