<?php

namespace App\Models;

use PDO;
use mysqli;

/**
 * Contact model
 */
class Contact extends \Core\Model {
    /**
     * Create database, table and add auto increment
     * 
     * @return self
     */
    protected static function getAll() {
        try {
            $conn = static::getDB();

            /**
             * Create contacts table
             */
            $createContactsTable = "CREATE TABLE IF NOT EXISTS contacts (
                id INT(3) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                userId INT(3) NOT NULL, 
                firstName VARCHAR(20) NOT NULL,
                lastName VARCHAR(30) NOT NULL,
                email VARCHAR(30) NOT NULL,
                address VARCHAR(30) NOT NULL,
                date TIMESTAMP
            )";

            /**
             * Add auto increment in contacts
             */
            mysqli_query($conn, 'ALTER TABLE contacts AUTO_INCREMENT=1');
            if ( mysqli_query($conn, $createContactsTable) ) {

            } else {
                echo 'Error creating Table: ' . mysqli_error($conn);
            }

            /**
             * Create phones table
             */
            $createPhonesTable = "CREATE TABLE IF NOT EXISTS phones (
                id INT(3) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                contactId INT(3) NOT NULL, 
                phone VARCHAR(30) NOT NULL,
                date TIMESTAMP
            )";

            /**
             * Add auto increment in phones
             */
            mysqli_query($conn, 'ALTER TABLE phones AUTO_INCREMENT=1');
            if ( mysqli_query($conn, $createPhonesTable) ) {

            } else {
                echo 'Error creating Table: ' . mysqli_error($conn);
            }

            return $conn;
        } catch (PDOException $e) {
            echo $e->getMeesage();
        }
    }

    /**
     * Add phones
     */
    protected static function addPhone($conn, $contactId) {
        if ( isset($_POST['phones']) ) {
            $phones = $_POST['phones'];
            
            foreach ( $phones as $phone ) {
                if ( !empty($phone) ) {
                    $stmt = $conn->prepare("INSERT INTO phones (contactId, phone) VALUES (?, ?)");
                    $stmt->bind_param('ii', $contactId, $phone);
                    $stmt->execute();

                    mysqli_stmt_close($stmt);
                } else {
                    return 'Fill out the missed fields';
                }
            }
        }
    }

    /**
     * Add contact
     */
    public static function addContact() {
        
        if ( isset($_POST['firstName'], $_POST['lastName'], $_POST['email'], $_POST['address']) ) {
            if ( !empty($_POST['firstName']) && !empty($_POST['lastName']) && !empty($_POST['email']) && !empty($_POST['address']) ) {
                $conn = self::getAll();
                
                if ( !isset($_SESSION) ) session_start();
                $userId = $_SESSION['userId'];
                $firstName = mysqli_real_escape_string($conn, $_POST['firstName']);
                $lastName = mysqli_real_escape_string($conn, $_POST['lastName']);
                $email = mysqli_real_escape_string($conn, $_POST['email']);
                $address = mysqli_real_escape_string($conn, $_POST['address']);
                
                $stmt = $conn->prepare("INSERT INTO contacts (userId, firstName, lastName, email, address) VALUES (?, ?, ?, ?, ?)");
                $stmt->bind_param('issss', $userId, $firstName, $lastName, $email, $address);
                $stmt->execute();
                $contactId = mysqli_insert_id($conn);
                mysqli_stmt_close($stmt);

                self::addPhone($conn, $contactId);

                mysqli_close($conn);
                
                exit(header("Location: /Home/$userId/index"));
            } else {
                return 'Fill out the missed fields';
            }
        }
    }
     
    /**
     * Get the user's contact list
     */
    public static function getContacts() {
        $conn = static::getDB();

        if ( !isset($_SESSION) ) session_start();
        $userId = mysqli_real_escape_string($conn, $_SESSION['userId']);
        $query = "SELECT contacts.id, userId, firstName, lastName, email, address, group_concat(`phone` separator ',') as phones FROM contacts INNER JOIN phones ON contacts.userId = '". $userId ."' AND contacts.id = phones.contactId group by phones.contactId";
        if ( $result = mysqli_query($conn, $query) ) {
            $rows = false;
            while ( $row = mysqli_fetch_array($result) ) {
                $row['phones'] = explode(',', $row['phones']);
                $rows[] = $row;
            }
            return $rows;
        }
    }

    /**
     * Edit contact
     */
    public static function editContact() {
        if ( isset($_POST['firstName'], $_POST['lastName'], $_POST['phones'], $_POST['email'], $_POST['address']) ) {
            if ( !empty($_POST['firstName']) && !empty($_POST['lastName']) && !empty($_POST['phones']) && !empty($_POST['email']) && !empty($_POST['address']) ) {
                $conn = static::getDB();
                
                $id = $_POST['id'];
                $firstName = mysqli_real_escape_string($conn, $_POST['firstName']);
                $lastName = mysqli_real_escape_string($conn, $_POST['lastName']);
                $email = mysqli_real_escape_string($conn, $_POST['email']);
                $address = mysqli_real_escape_string($conn, $_POST['address']);
                
                $user = $_SESSION['userId'];
                $query = "UPDATE contacts SET firstName = '". $firstName ."', lastName = '". $lastName ."', email = '". $email ."', address = '". $address ."' WHERE id = '". $id ."'";
                $query_2 = "DELETE FROM phones WHERE contactId = '". $id ."'";
                if ( $result = mysqli_query($conn, $query) ) {
                    if ( $result_2 = mysqli_query($conn, $query_2) ) {
                        self::addPhone($conn, $id);
                        exit(header("Location: /home/$user/index"));
                    }
                }
            } else {
                return 'Fill out the missed fields';
            }
        }
    }

    /**
     * Delete contact
     */
    public static function deleteContact() {
        $conn = static::getDB();

        if ( !isset($_SESSION) ) session_start();
        $user = $_SESSION['userId'];
        $id = $_GET['id'];
        $result = $conn -> query("DELETE FROM contacts WHERE id='$id'");
        $result = $conn -> query("DELETE FROM phones WHERE contactId = '". $id ."'");

        exit(header("Location: /home/$user/index"));
    }

    /**
     * Search contacts by first name, last name, email
     */
    public static function searchContacts() {
        if ( !isset($_SESSION) ) session_start();
        $userId = $_SESSION['userId'];
        
        $conn = static::getDB();

        $searchContacts = mysqli_real_escape_string($conn, $_GET['searchContacts']);
        $query = "SELECT contacts.id, userId, firstName, lastName, email, address, group_concat(`phone` separator ',') as phones FROM contacts INNER JOIN phones ON contacts.userId = '". $userId ."' AND (contacts.firstName LIKE '%{$searchContacts}%' OR contacts.lastName LIKE '%{$searchContacts}%' OR contacts.email LIKE '%{$searchContacts}%') AND contacts.id = phones.contactId group by phones.contactId";
        if ( $result = mysqli_query($conn, $query) ) {
            $contacts = false;
            while ( $row = mysqli_fetch_array($result) ) {
                $row['phones'] = explode(',', $row['phones']);
                $contacts[] = $row;
            }
            return $contacts;
        }
    }
}