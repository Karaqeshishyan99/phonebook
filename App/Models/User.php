<?php 

namespace App\Models;

use PDO;
use mysqli;
use App\Config;

/**
 * User model
 */
class User extends \Core\Model {
    /**
     * Create database, table and add auto increment
     * 
     * @return self
     */
    protected static function getAll() {
        try {
            $conn = static::getDB();

            /**
             * Create table
             */
            $createTable = "CREATE TABLE IF NOT EXISTS users (
                id INT(3) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
                fullName VARCHAR(30) NOT NULL,
                userName VARCHAR(20) NOT NULL,
                phone VARCHAR(20) NOT NULL,
                password VARCHAR(20) NOT NULL,
                date TIMESTAMP
            )";

            /**
             * Add auto increment
             */
            mysqli_query($conn, 'ALTER TABLE users AUTO_INCREMENT=1');
            if ( mysqli_query($conn, $createTable) ) {

            } else {
                echo 'Error creating Table: ' . mysqli_error($conn);
            }
            return $conn;
        } catch (PDOException $e) {
            echo $e->getMeesage();
        }
    }

    /**
     * Create user
     */
    public static function createUser() {
        if ( isset($_POST['fullName'], $_POST['userName'], $_POST['phone'], $_POST['password']) ) {
            if ( !empty($_POST['fullName']) && !empty($_POST['userName']) && !empty($_POST['phone']) && !empty($_POST['password']) ) {
                $conn = self::getAll();
                
                $fullName = $_POST['fullName'];
                $userName = $_POST['userName'];
                $phone = $_POST['phone'];
                $password = $_POST['password'];

                $stmt = $conn->prepare("INSERT INTO users (fullName, userName, phone, password) VALUES (?, ?, ?, ?)");
                $stmt->bind_param('ssis', $fullName, $userName, $phone, $password);
                $stmt->execute();
                exit(header('Location: /users/signIn'));

                mysqli_stmt_close($stmt);
                mysqli_close($conn);
            } else {
                return 'Fill out the missed fields';
            }
        }
    }

    /**
     * Check user
     */
    public static function checkUser() {
        if ( isset($_POST['userName'], $_POST['password']) ) {
            if ( !empty($_POST['userName']) && !empty($_POST['password']) ) {
                $conn = static::getDB();
        
                $userName = mysqli_real_escape_string($conn, $_POST['userName']);
                $password = mysqli_real_escape_string($conn, $_POST['password']);
                
                $query = "SELECT * FROM users WHERE userName = '". $userName ."' AND password = '". $password ."'";
                if ( $result = mysqli_query($conn, $query) ) {
                    if ( mysqli_num_rows($result) == 0 ) {
                        return 'username or password is incorrect';
                    } else {
                        if ( !isset($_SESSION) ) session_start();
                        $user = $result->fetch_assoc();
                        $user = $user['id'];
                        $_SESSION['userId'] = $user;
                        exit(header("Location: /home/$user/index"));
                    }
                } else {
                    return 'go to the link below and sign up';
                }
            } else {
                return 'Fill out the missed fields';
            }
        }
    }

    /**
     * Get user 
     */
    public static function getUser() {
        $conn = static::getDB();

        $user = $_SESSION['userId'];
        $query = "SELECT * FROM users WHERE id = $user";
        if ( $result = mysqli_query($conn, $query) ) {
            $user = false;
            while ( $row = mysqli_fetch_array($result) ) {
                $user = $row;
            }
            return $user;
        }
    }

    /**
     * Edit user 
     */
    public static function editUser() {
        if ( isset($_POST['fullName'], $_POST['userName'], $_POST['phone'], $_POST['password']) ) {
            if ( !empty($_POST['fullName']) && !empty($_POST['userName']) && !empty($_POST['phone']) && !empty($_POST['password']) ) {
                $conn = static::getDB();

                $fullName = mysqli_real_escape_string($conn, $_POST['fullName']);
                $userName = mysqli_real_escape_string($conn, $_POST['userName']);
                $phone = mysqli_real_escape_string($conn, $_POST['phone']);
                $password = mysqli_real_escape_string($conn, $_POST['password']);
                
                $user = $_SESSION['userId'];
                $query = "UPDATE users SET fullName = '". $fullName ."', userName = '". $userName ."', phone = '". $phone ."', password = '". $password ."' WHERE id = $user";
                if ( $result = mysqli_query($conn, $query) ) {
                    exit(header("Location: /users/$user/myProfile"));
                }
            } else {
                return 'Fill out the missed fields';
            }
        }
    }
}