<?php 
    if ( !isset($_SESSION) ) session_start();
    if ( isset($_SESSION['userId']) ) {
        $userId = $_SESSION['userId'];
        $links = [ 
            "<li class='nav-item'><a class='nav-link' href='/Home/$userId/index'>Home</a></li>",
            "<li class='nav-item'><a class='nav-link' href='/Users/$userId/myProfile'>My Profile</a></li>",
            "<li class='nav-item'><a class='nav-link' href='/Users/$userId/logOut'>Log Out</a></li>" 
        ];
    } else {
        $links = false;
    }
?>
<?php if ( $links ): ?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand text-uppercase font-italic text-info" href="/Home/<?php echo $userId ?>/index">
        <img src="/assets/images/logo.jpg" class="logo" />Phonebook</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
        <ul class="navbar-nav">
            <?php foreach ( $links as $link ) { 
                echo $link . "\n";
            } ?>
        </ul>
    </div>
</nav>
<?php endif ?>