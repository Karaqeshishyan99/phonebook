<section class="section">
    <div class="container">
        <form class="signUp col-lg-6" action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
            <h1 class="title">Sign In</h1>
            <input type="text" name="userName" placeholder="User name" />
            <input type="password" name="password" placeholder="Password" />
            <div class="error mb-2"><span><?php echo $users ?><span></div>
            <button class="button" type="submit">Sign In <i class="fas fa-hand-point-right"></i></button>
            <a href="/users/signUp" class="mt-3">Sign Up</a>
        </form>
    </div>
</section>