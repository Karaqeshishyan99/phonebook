<section class="section">
    <div class="container">
        <form class="signUp col-lg-6" action="/users/signUp" method="post">
            <h1 class="title">Sign Up</h1>
            <input type="text" name="fullName" placeholder="Full name" />
            <input type="text" name="userName" placeholder="User name" />
            <input type="number" name="phone" placeholder="Phone" />
            <input type="password" name="password" placeholder="Password" />
            <div class="error mb-2"><span><?php echo $users ?><span></div>
            <button class="button" type="submit">Sign Up <i class="fas fa-hand-point-right"></i></button>
            <a href="/users/signIn" class="mt-3">Sign In</a>
        </form>
    </div>
</section>