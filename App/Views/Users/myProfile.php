<section>
    <div class="container">
        <div class="row justify-content-start my-3">
            <div class="mx-3">
                <img src="/assets/images/user.jpg" alt="user">
            </div>
            <div>
                <h3 class="mt-2">User Name: <span class="user-field-value"><?php echo $user["userName"]?></span>
                </h3>
                <h3 class="mt-2">Full Name: <span class="user-field-value"><?php echo $user['fullName']?></span>
                </h3>
                <h3 class="mt-2">Phone: <span class="user-field-value"><?php echo $user['phone']?></span>
                </h3>
                <h3 class="mt-2">Date of creation: <span class="user-field-value"><?php echo $user['date']?></span>
                </h3>
                <a class="button userEdit mt-3"
                    href="/users/<?php echo $_SESSION['userId']; ?>/edit?userName=<?php echo $user["userName"].'&fullName='.$user["fullName"].'&phone='.$user["phone"]; ?>">
                    <i class="fa fa-edit"></i>Edit
                </a>
            </div>
        </div>
    </div>
</section>