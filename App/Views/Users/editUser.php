<section class="section2">
    <div class="container">
        <form class="signUp col-lg-6" action="/users/<?php echo $_SESSION['userId']; ?>/edit" method="post">
            <h1 class="title">Edit User</h1>
            <input type="text" name="fullName" placeholder="Full name"
                value="<?php echo isset($_GET['fullName']) ? $_GET['fullName'] : ''; ?>" />
            <input type="text" name="userName" placeholder="User name"
                value="<?php echo isset($_GET['userName']) ? $_GET['userName'] : ''; ?>" />
            <input type="number" name="phone" placeholder="Phone"
                value="<?php echo isset($_GET['phone']) ? $_GET['phone'] : ''; ?>" />
            <input type="password" name="password" placeholder="password" />
            <div class="error mb-2"><span><?php echo $user ?><span></div>
            <button class="button" type="submit">Edit<i class="fas fa-hand-point-right"></i></button>
        </form>
    </div>
</section>