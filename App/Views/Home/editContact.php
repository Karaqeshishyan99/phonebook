<section class="section2">
    <div class="container">
        <form class="signUp col-lg-6" action="/home/<?php echo $_SESSION['userId']; ?>/editContact" method="post">
            <h1 class="title">Edit Contact</h1>
            <input type="hidden" name="id" value="<?php echo $_POST['id']; ?>">
            <input type="text" name="firstName" placeholder="First name"
                value="<?php echo isset($_POST['firstName'])?$_POST['firstName']:''; ?>" />
            <input type="text" name="lastName" placeholder="Last name"
                value="<?php echo isset($_POST['lastName'])?$_POST['lastName']:''; ?>" />
            <div class="phone-numbers">
                <?php if ( isset($_POST['phones']) ): ?>
                <?php $i = 0; foreach ( $_POST['phones'] as $phone): $i++ ?>
                <div>
                    <input type="number" name="phones[]" placeholder="Phone" class="delete-input"
                        value="<?php echo $phone;?>" /><?php if ( $i > 1 ): ?><i
                        class="fas fa-minus-circle delete-input-icon"
                        onclick="deleteIcon(this, event)"></i><?php endif ?>
                </div>
                <?php endforeach ?>
                <?php endif ?>
            </div>
            <span class="add-phone">Add Phone</span>
            <input type="email" name="email" placeholder="email"
                value="<?php echo isset($_POST['email'])?$_POST['email']:''; ?>" />
            <input type="address" name="address" placeholder="Address"
                value="<?php echo isset($_POST['address'])?$_POST['address']:''; ?>" />
            <div class="error mb-2"><span><?php echo $contact ?><span></div>
            <button class="button" name="submit" type="submit">Edit<i class="fas fa-hand-point-right"></i></button>
        </form>
    </div>
</section>