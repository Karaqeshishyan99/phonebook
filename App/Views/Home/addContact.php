<section class="section2">
    <div class="container">
        <form class="signUp col-lg-6" action="/home/<?php echo $_SESSION['userId']; ?>/addContact" method="post">
            <h1 class="title">Add Contact</h1>
            <input type="text" name="firstName" placeholder="First name" />
            <input type="text" name="lastName" placeholder="Last name" />
            <div class="phone-numbers">
                <input type="number" name="phones[]" placeholder="Phone" />
            </div>
            <span class="add-phone">Add Phone</span>
            <input type="email" name="email" placeholder="email" />
            <input type="address" name="address" placeholder="Address" />
            <div class="error mb-2"><span><?php echo $contact ?><span></div>
            <button class="button" type="submit" name="submit">Add<i class="fas fa-hand-point-right"></i></button>
        </form>
    </div>
</section>