<section class="section2">
    <div class="container">
        <div class="row justify-content-between mx-0 my-3">
            <form class="form-inline my-2 my-lg-0" method="get" action="/Home/<?php echo $_SESSION['userId']; ?>/index">
                <input class="form-control mr-sm-2 mb-0" type="search" name="searchContacts"
                    placeholder="Search Contacts" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            <div class="add-contact">
                <a class='addContact' href='/Home/<?php echo $_SESSION['userId']; ?>/addContact'>
                    <div class="addContact-hidden">Add Contact</div><i class="fas fa-plus addContact-icon"></i>
                </a>
            </div>
        </div>
    </div>
    <?php if ( $contacts ): ?>
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Phone(s)</th>
                <th scope="col">Email</th>
                <th scope="col">Address</th>
                <th scope="col" colspan="2">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ( $contacts as $contact ): ?>
            <tr>
                <td><?php echo htmlspecialchars($contact['id']) ?></td>
                <td><?php echo htmlspecialchars($contact['firstName']) ?></td>
                <td><?php echo htmlspecialchars($contact['lastName']) ?></td>
                <td>
                    <?php foreach ( $contact['phones'] as $phone ): ?>
                    <div>
                        <?php echo htmlspecialchars($phone); $phones = $phone ?>
                    </div>
                    <?php endforeach ?>
                </td>
                <td><?php echo htmlspecialchars($contact['email']) ?></td>
                <td><?php echo htmlspecialchars($contact['address']) ?></td>
                <td>
                    <form action="/Home/<?php echo $_SESSION['userId']; ?>/editContact" method="post">
                        <input type="hidden" name="id" value="<?php echo htmlspecialchars($contact['id']) ?>">
                        <input type="hidden" name="firstName"
                            value="<?php echo htmlspecialchars($contact['firstName']) ?>">
                        <input type="hidden" name="lastName"
                            value="<?php echo htmlspecialchars($contact['lastName']) ?>">
                        <?php foreach ( $contact['phones'] as $phone ): ?>
                        <input type="hidden" name="phones[]" value="<?php echo htmlspecialchars($phone) ?>">
                        <?php endforeach ?>
                        <input type="hidden" name="email" value="<?php echo htmlspecialchars($contact['email']) ?>">
                        <input type="hidden" name="address" value="<?php echo htmlspecialchars($contact['address']) ?>">
                        <button type="submit" class="button ContactsEdit">
                            <i class="fa fa-edit Contacts-icon"></i>Edit
                        </button>
                    </form>
                </td>
                <td>
                    <a class="button ContactsDelete"
                        href="/home/<?php echo $_SESSION['userId']; ?>/deleteContact?id=<?php echo $contact['id']; ?>">
                        <i class="fas fa-minus-circle Contacts-icon"></i>Delete
                    </a>
                </td>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table>
    <?php endif ?>
</section>