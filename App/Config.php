<?php 

namespace App;

/**
 * Application configuration
 */
class Config {
    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'localhost';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'phonebook';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'root';

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = '';

    /**
     * Show or hide error message on screen
     * @var boolean
     */
    const SHOW_ERRORS = false;
}