<?php

namespace App\Controllers;

use \Core\View;
use App\Models\User;

/**
 * Users controller
 */
class Users extends \Core\Controller {
    /**
     * Before filter
     *
     * @return void
     */
    protected function before() {
        if ( !isset($_SESSION) ) session_start();
        if ( $this->route_params['action'] == 'signUp' || $this->route_params['action'] == 'signIn' ) {
            if ( isset($_SESSION['userId']) ) { 
                exit(header("Location: /"));
            }
        } elseif ( $this->route_params['action'] == 'logOut' ) {
            if ( !isset($_SESSION['userId']) ) {
                exit(header("Location: /users/signIn"));
            }
        } else {
            if ( isset($_SESSION['userId']) ) {
                $userId = $_SESSION['userId'];
                if ( $this->route_params['id'] != $userId ) {
                    return false;
                }
            } else {
                exit(header("Location: /users/signIn"));
            }
        }
    }

    /**
     * After filter
     *
     * @return void
     */
    protected function after() {
        // echo " (after)";
    }

    /**
     * Sign Up 
     * 
     * @return void
     */
    public function signUpAction() {
        $users = User::createUser();
        
        View::render('Users/signUp.php', [
            'users' => $users
        ]);
    }

    /**
     * Sign In 
     * 
     * @return void
     */
    public function signInAction() {
        $users = User::checkUser();
        
        View::render("Users/signIn.php", [
            'users' => $users
        ]);
    }

    /**
     * log out
     * 
     * @return void
     */
    public function logOutAction() {
        unset($_SESSION['userId']);
        session_destroy();
        exit(header('Location: /users/signIn'));
    }

    /**
     * Show user profile page
     * 
     * @return void
     */
    public function myProfileAction() {
        $user = User::getUser();

        View::render("Users/myProfile.php", [
            "user" => $user
        ]);
    }
    
    /**
     * Show the edit page
     *
     * @return void
     */
    public function editAction() {
        $user = User::editUser();
        
        View::render("Users/editUser.php", [
            "user" => $user
        ]);
    }
}