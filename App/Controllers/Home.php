<?php

namespace App\Controllers;

use \Core\View;
use App\Models\Contact;

/**
 * Home controller
 */
class Home extends \Core\Controller {
    /**
     * Before filter
     *
     * @return void
     */
    protected function before() {
        if ( !isset($_SESSION) ) session_start();
        if ( isset($_SESSION['userId']) ) { 
            $userId = $_SESSION['userId'];
            if ( !isset($this->route_params['id']) ) {
                exit(header("Location: /home/$userId/index"));
            }
            if ( $this->route_params['id'] != $userId ) {
                return false;
            }
        } else {
            exit(header("Location: /users/signIn"));
        }
    }

    /**
     * After filter
     *
     * @return void
     */
    protected function after() {
        // echo " (after)";
    }

    /**
     * Show the index page
     * 
     * @return void
     */
    public function indexAction() {
        if ( isset($_GET['searchContacts']) ) {
            if ( !empty($_GET['searchContacts']) ) {
                $contacts = Contact::searchContacts();
            } else {
                $contacts = Contact::getContacts();
            }
        } else {
            $contacts = Contact::getContacts();
        }

        View::render('Home/index.php', [
            'contacts' => $contacts
        ]);
    }

    /**
     * Show the create contact page
     * 
     * @return void
     */
    public function addContactAction() {
        $contact = Contact::addContact();

        View::render('Home/addContact.php', [
            'contact' => $contact
        ]);
    }

    /**
     * Show the create contact page
     * 
     * @return void
     */
    public function editContactAction() {
        $contact = null;
        if ( isset($_POST['submit']) ) {
            $contact = Contact::editContact();
        } 
        // else {
        //     $userId = $_SESSION['userId'];
        //     exit(header("Location: /home/$userId/index"));
        // }
        View::render('Home/editContact.php', [
            'contact' => $contact
        ]);
    }

    /**
     * Show the create contact page
     * 
     * @return void
     */
    public function deleteContactAction() {
        if ( isset($_GET['id']) ) {
            $contact = Contact::deleteContact();
        } else {
            $userId = $_SESSION['userId'];
            exit(header("Location: /home/$userId/index"));
        }

        View::render('Home/index.php', [
            'contact' => $contact
        ]);
    }
}